import feedparser
import markovify
from settings import FEEDS
from random import randint

def get_titles(feeds):
    titles = []
    for feed in feeds:
        rss = feedparser.parse(feed)
        for entry in rss.entries:
            if '|' in entry:
                entry = entry.split('|')[0]
            titles.append(entry.title.replace("/n", ""))
    return titles

def create_news():
    titles = get_titles(FEEDS)
    # we vary the state size on each generation
    title_model = markovify.Text(titles, state_size=randint(2, 3))
    markovtitle = ''
    while markovtitle == '':
        markovtitle = title_model.make_sentence()
    return markovtitle
