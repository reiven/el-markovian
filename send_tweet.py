import tweepy
from settings import consumer_key, consumer_secret, access_token, access_secret
from news_getter import create_news

# connect to twitter api
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = tweepy.API(auth)

tosend = create_news()
api.update_status(tosend)
